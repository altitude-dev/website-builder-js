# Website Builder JS
This initial version provides a website builder with SortableJS, vanillaJS, and StimulusJS. It uses a derivative of Bootstrap for CSS. Once the bugs are fixed, it will be available for other CSS frameworks like Tailwind.

# How to install ?
Just place the folder in root and launch the index.html in local or live server visual
/rootFolder/Files..

# Documentation SORTABLEJS
https://github.com/SortableJS/Sortable

# Bug need to fix (help wanted)
- When an item is deleted, or when an item is moved, a "blind" effect occurs.

# Creator
Florian.prache from Linkedin https://www.linkedin.com/in/altitude-dev/