/**
 * Andromede Builder
 * @copyright ALTITUDE DEV, Florian Prache
 */
import Sortable from '/plugins/sortablejs/modular/sortable.complete.esm.js';
import { Application, Controller } from '/plugins/Stimulus.js';
import Builder from '/builder/fse/BuilderController.js';

let Style = {};
const utils = Sortable.utils;
const StimulusApp = Application.start();
let SelectedNode = false;
let BuilderAPP = false;
let node = false;

/**
 * StyleManager
 */
StimulusApp.register("stylemanager", class extends Controller {
    connect(){
        BuilderAPP = Builder.Builder.Andromede;
    }

    nodeClassChange(e){
        let value = e.target.value;
        Style.Andromede.setClassNode(SelectedNode, value);
    }

    nodeIDChange(e){
        let value = e.target.value;
        Style.Andromede.setIDNode(SelectedNode, value);
    }

});

Style.Andromede = {

    /**
     * InitSelected Node
     */
    nodeisSelected: function(node){
        Style.Andromede.getClass(node);
        SelectedNode = node;
        const tablist = document.getElementById('leftArea');
        if (tablist) {
            const buttons = tablist.querySelectorAll('.active');
            buttons.forEach(button => {
                button.classList.remove('active');
            });
        }
        isnoded.style.display = 'block';
        isnoded.classList.add('active');
        tab2.classList.add('active');
    },

    /**
     * Get All Class from Node
     */
    getClass: function(node){
        let removedClassInContent = [
            "hoveredParent",
            "selectedBlock"
        ];
        let classes = Array.from(node.item.classList).filter(cls => !removedClassInContent.includes(cls));
        let inputClassNode = document.getElementById('nodeClass');
        inputClassNode.value = classes.join(' ');
    },

    /**
     * Set a CSS for a Node
     */
    setCssNode: function(node){
       utils.css(node.item, 'background-color', 'red');
    },

    /**
     * Set Class for a Node
     */
    setClassNode: function(node, values){
        node.item.className = '';
        const classesToAdd = values.split(' ');
        classesToAdd.forEach(className => {
            if (className.trim() !== '') {
                node.item.classList.add(className.trim());
            }
        });
        node.item.classList.add('selectedBlock');
        BuilderAPP.showContextMenu(node, node.item);
    },

    /**
     * Set ID for a Node
     */
    setIDNode: function(node, values){
        const idToAdd = values.trim();
        if (idToAdd !== '') {
            node.item.id = idToAdd;
        } else {
            node.item.removeAttribute('id');
        }
    }

}

export default {Style}