/**
 * Andromede Builder
 * @copyright ALTITUDE DEV, Florian Prache
 */
import Sortable from '/plugins/sortablejs/modular/sortable.complete.esm.js';
import { Application, Controller } from '/plugins/Stimulus.js';
import Builder from '/builder/fse/BuilderController.js';

let Backup = {};
const StimulusApp = Application.start();
let SelectedNode = false;
let BuilderAPP = false;

StimulusApp.register("backupmanager", class extends Controller {
    connect(){
        BuilderAPP = Builder.Builder.Andromede;
    }
});

let backupStorage = {};

Backup.Andromede = {
    init: function(htmlContent) {
        const key = 'Andromede';
        if (!backupStorage[key]) {
            backupStorage[key] = [];
        }
        backupStorage[key].push({
            timestamp: new Date().toLocaleString(),
            content: htmlContent
        });
        let backups = JSON.parse(localStorage.getItem(key)) || [];
        if (backups.length > 0) {
            let lastBackup = backups[backups.length - 1];
            if (lastBackup.content === htmlContent) {
                return;
            }
        }
        localStorage.setItem(key, JSON.stringify(backupStorage[key]));
    },

    currentBackupIndex: 1,

    clearAllBackups: function() {
        const key = 'Andromede';
        delete backupStorage[key];
        localStorage.removeItem(key);
    },

    undo: function() {
        const key = 'Andromede';
        let backups = JSON.parse(localStorage.getItem(key)) || [];
        if (this.currentBackupIndex === -1) {
            this.currentBackupIndex = backups.length - 1;
        } else {
            this.currentBackupIndex--;
            if (this.currentBackupIndex < 0) {
                this.currentBackupIndex = backups.length - 1;
            }
        }
        let currentBackup = backups[this.currentBackupIndex];
        return currentBackup ? currentBackup.content : null;
    },

    setBackup: function(content){
        BuilderAPP.setFrameContent(content);
    },

};

document.addEventListener('keydown', function(event) {
    if (event.key === 'z' && (event.ctrlKey || event.metaKey)) {
        let UndoGet = Backup.Andromede.undo();
        Backup.Andromede.setBackup(UndoGet);
    }
});

export default {Backup}