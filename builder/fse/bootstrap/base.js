/**
 * Andromede Builder
 * @copyright ALTITUDE DEV
 */

let Kits = {}

Kits.base = [
    {
        'name': 'section',
        'class': false,
        'category': 'base',
        'html': `<section></section>`
    },
    {
        'name': 'container-fluid',
        'class': 'container-fluid',
        'category': 'base',
        'html': `<div class="container-fluid"></div>`
    },
    {
        'name': 'container',
        'class': 'container',
        'category': 'base',
        'html': `<div class="container"></div>`
    },
    {
        'name': 'row',
        'class': 'row',
        'category': 'base',
        'html': `<div class="row"></div>`
    },
    {
        'name': 'col-lg-3',
        'class': 'col-lg-3',
        'category': 'base',
        'html': `<div class="col-lg-3"></div>`
    },
    {
        'name': 'col-lg-6',
        'class': 'col-lg-6',
        'category': 'base',
        'html': `<div class="col-lg-6"></div>`
    },
    {
        'name': 'Image',
        'class': '',
        'category': 'widgets',
        'html': `<img src="https://shared.akamai.steamstatic.com/store_item_assets/steam/apps/1931730/ss_a5d10c3d41eec484450debbbfd8918c2eefc60bb.1920x1080.jpg?t=1726476862" alt="">`
    },
    {
        'name': 'Iframe',
        'class': '',
        'category': 'widgets',
        'html': `<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Ft3zEuP1elk?si=s7H7mKQ0P_CzTced" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`
    },
    {
        'name': 'Paragraph',
        'class': '',
        'category': 'base',
        'html': `<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque gravida felis at faucibus efficitur. Sed sagittis elit metus, id molestie ante facilisis vitae. Curabitur fermentum mollis faucibus. Suspendisse semper nibh eget tellus varius finibus. Integer vulputate felis non iaculis ultricies. Praesent ac massa a risus ultricies venenatis. Aenean vitae lobortis risus. Quisque ut leo neque. Nam non turpis odio. Etiam sed porta nunc, at iaculis nulla. Praesent eget dui lacus. Quisque nec pellentesque enim, at volutpat sem. Ut erat est, imperdiet ac metus vel, lacinia euismod felis. Vestibulum euismod lacus mi, in dictum ligula bibendum eget. Pellentesque sed consequat felis. Nunc tempor nec ipsum elementum luctus. </p>`
    }
];

export default {Kits}