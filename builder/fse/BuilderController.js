/**
 * Andromede Builder
 * @copyright ALTITUDE DEV, Florian Prache
 */

/**
 * Builder Dependancies
 */
import Sortable from '/plugins/sortablejs/modular/sortable.complete.esm.js';
import { Application, Controller } from '/plugins/Stimulus.js';
import Style from '/builder/fse/StylesController.js';
import Backup from '/builder/fse/BackupController.js';

/**
 * Kits
 */
import Kits from '/builder/fse/bootstrap/base.js';
//import Sections from '/builder/fse/bootstrap/sections.js';

/**
 * Configuration
 */
let Builder      = {};
let StyleBuilder = Style.Style.Andromede;
let KitsBuilder  = Kits.Kits.base;
//let SectionsBuilder  = Sections.Sections.base;
let selectedElement  = false;
let selectedElementNode  = false;
let selectedElementNodeID = false;
let assetsKitsUI = document.getElementById('assetsKits');
let BuilderArea  = document.getElementById('andromede-editor');
let editorBlock  = document.getElementById('editor_');
const SpeedDrop  = 150;
let sortableArea = false;
let SortableSections = false;
let SortableKits = false;
let timeoutIds   = [];
const modalBuilder = document.getElementById("modalBuilder");
const StimulusApp = Application.start();

/**
 * BuilderManager
 */
StimulusApp.register("buildermanager", class extends Controller {
    connect(){
        // console.warn('🔥 ​🇧​​🇺​​🇮​​🇱​​🇩​​🇪​​🇷​​🇲​​🇦​​🇳​​🇦​​🇬​​🇪​​🇷 ​🇮​​🇸​ ​🇷​​🇪​​🇦​​🇩​​🇾 🔥');
        // console.log(Builder.Andromede);
        // console.warn('💧 ​🇸​​🇹​​🇾​​🇱​​🇪​​🇲​​🇦​​🇳​​🇦​​🇬​​🇪​​🇷​ ​🇮​​🇸​ ​🇷​​🇪​​🇦​​🇩​​🇾 💧​');
        // console.log(StyleBuilder);
    }

    eclatedView(){
        Builder.Andromede.removeSelected();
        document.body.classList.toggle('eclated');
    }

    leftbarmoove(){
        Builder.Andromede.removeSelected();
        document.body.classList.toggle('fullwidth');
        document.getElementById('leftArea').classList.toggle('removedleft');
    }

    eraseBuilder(){
        Builder.Andromede.eraseBuilder();
    }

    downloadCanva() {
        if (BuilderArea) {
            Builder.Andromede.notificationSend('🔥Téléchargement de votre contenu', false);
            const content = BuilderArea.innerHTML;
            const blob = new Blob([content], { type: 'text/html' });
            const url = URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = url;
            a.download = 'design.html';
            document.body.appendChild(a);
            a.click();
            URL.revokeObjectURL(url);
            document.body.removeChild(a);
        }
    }

    openhtml(){
        Builder.Andromede.openEditorHTML();
    }

    closehtml(){
        editorBlock.classList.toggle('show_editor');
    }

    applyhtml(){
        Builder.Andromede.saveHTML();
    }

    tinyedition(){
        Builder.Andromede.BlockopenEditorTiny(selectedElementNode);
    }

    openCredits(){
        Builder.Andromede.Credits();
    }

    addachildrenbefore(e){

        // const choicedModal = document.createElement('div');
        // const rect = selectedElement.getBoundingClientRect();
        // choicedModal.innerHTML = `<div class="choicedkit" id="contentchoiced">content</div>`;
        // choicedModal.style.position = 'absolute';
        // choicedModal.style.top = `${rect.bottom + window.scrollY + 30}px`;
        // choicedModal.style.left = `${rect.left + window.scrollX + rect.width / 2}px`;
        // choicedModal.style.transform = 'translateX(-50%)';
        // choicedModal.style.transform = 'translateX(-50%)';
        // choicedModal.style.width = '400px';
        // document.body.appendChild(choicedModal);


        // const targetElement = selectedElement;
        // const newElement = document.createElement('div');
        // newElement.innerHTML = `OK`;
        // targetElement.insertAdjacentElement('afterend', newElement);
    }
});

/**
 * CodeMirror 5
 */
const editor = CodeMirror.fromTextArea(document.getElementById('code_editor'), {
    mode: 'htmlmixed',
    autoIndent: true,
    indentUnit:1,
    tabSize:1,
    enterMode: "keep",
    tabMode: "shift",
    lineNumbers:true,
    lineWrapping: true,
    theme:'material'
});

/**
 * BUILDER
 */
Builder.init = function() {
    Builder.Andromede.init();
};

Builder.Andromede = {

    /**
     * Init the Builder and Libs
     */
    init: function() {
       this.loadDocument();
       this.initComponents();
       this.shortcuts();
    },

    /**
     * Load Async Document
     */
    loadDocument: function(){
        BuilderArea.innerHTML = '';
        async function getData() {
            const url = 'content-bootstrap.html';
            try {
                const response = await fetch(url);
                if (!response.ok) {
                    throw new Error(`Response status: ${response.status}`);
                }

                const html      = await response.text();
                const mainContent = `<andromede>${html}</andromede>`;

                BuilderArea.innerHTML = mainContent;
                Builder.Andromede.coreFrame();
                Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            } catch (error) {
                console.error(error.message);
            }
        }
        getData();
    },

    CurrentGrab:function(){
        document.body.classList.toggle('eclated');
    },

    /**
     * Init ComponentsList
     */
    initComponents: function(){

        let assetsKits = document.getElementById('assetsKits');
        assetsKits.innerHTML = '';
        SortableKits = new Sortable(assetsKits,{
            group: {
                name: 'nested',
                pull: 'clone',
                put: false
            },
            sort:false,
            animation: SpeedDrop,
            fallbackOnBody: true,
            swapThreshold: 1.0,
            ghostClass: 'grabbingElement',
            chosenClass: "sortableChosen",  
	        dragClass: "sortableDrag",
            forceFallback: true,
            scroll: true,
            bubbleScroll: false,
            fallbackOnBody: true,
            dragoverBubble: false,
            bubble:false,
            forceFallback: true,
            onStart: function (event) {
                Builder.Andromede.CurrentGrab();
            },
            onEnd: function (e) {
                if(e.to.id != 'assetsKits'){
                    let droppedElement = e.item;
                    let dataContentValue = droppedElement.getAttribute('data-content');
                    if (dataContentValue) {
                        let tempDiv = document.createElement('div');
                        tempDiv.innerHTML = dataContentValue.trim();
                        let newElement = tempDiv.firstChild;
                        droppedElement.parentNode.replaceChild(newElement, droppedElement);
                    }
                    Builder.Andromede.coreFrame();
                }
                setTimeout(() => {
                    Builder.Andromede.CurrentGrab();
                }, 600);
            }
        });

        /**
         * Generate kits availables from file and SortList
         */
        KitsBuilder.forEach(item => {
            let kitElement = document.createElement('div');
            kitElement.innerHTML = item.name;
            kitElement.setAttribute('data-content', item.html.trim());
            if(item.category){
                assetsKitsUI.appendChild(kitElement);
            }
        });

        let assetsSections = document.getElementById('sectionsList');
        assetsSections.innerHTML = '';
        SortableSections = new Sortable(assetsSections,{
            group: {
                name: 'nested',
                pull: 'clone',
                put: false
            },
            sort:false,
            animation: SpeedDrop,
            fallbackOnBody: true,
            swapThreshold: 1.0,
            ghostClass: 'grabbingElement',
            chosenClass: "sortableChosen",  
	        dragClass: "sortableDrag",
            forceFallback: true,
            scroll: true,
            bubbleScroll: false,
            fallbackOnBody: true,
            bubble:false,
            dragoverBubble: false,
            forceFallback: true,
            onStart: function (event) {
                Builder.Andromede.CurrentGrab();
            },
            onEnd: function (e) {
                if(e.to.id != 'sectionsList'){
                    let droppedElement = e.item;
                    let dataContentValue = droppedElement.getAttribute('data-content');
                    let dataCssValue = droppedElement.getAttribute('data-css');
                    if (dataContentValue) {
                        let tempDiv = document.createElement('div');
                        tempDiv.innerHTML = dataContentValue.trim();
                        let newElement = tempDiv.firstChild;
                        droppedElement.parentNode.replaceChild(newElement, droppedElement);
                    }
                    if (dataCssValue) {
                        let styleElement = document.createElement('style');
                        styleElement.type = 'text/css';
                        styleElement.innerHTML = dataCssValue.trim();
                        document.head.appendChild(styleElement);
                    }
                    Builder.Andromede.coreFrame();
                }
                Builder.Andromede.CurrentGrab();
            }
        });

        let Sections = [];
        async function getCustomBlocks() {
            const urlblocks = 'blocks/blocks.json';
            try {
                const responseblocks = await fetch(urlblocks);
                if (!responseblocks.ok) {
                    throw new Error(`Response status: ${responseblocks.status}`);
                }
                const blocksBuilder = await responseblocks.json();
                await getCustomBlocksLib(blocksBuilder.blocks);
            } catch (error) {
                console.error(error.message);
            }
        }

        async function getCustomBlocksLib(blockImport) {
            for (const block of blockImport) {
                const jsonUrl = `blocks/${block}/block.json`;
                const htmlUrl = `blocks/${block}/block.html`;
                const cssUrl = `blocks/${block}/block.css`;
                try {
                    const [responseJson, responseHtml, responseCss] = await Promise.all([
                        fetch(jsonUrl),
                        fetch(htmlUrl),
                        fetch(cssUrl)
                    ]);
                    if (!responseJson.ok) {
                        throw new Error(`JSON response status: ${responseJson.status}`);
                    }
                    if (!responseHtml.ok) {
                        throw new Error(`HTML response status: ${responseHtml.status}`);
                    }
                    if (!responseCss.ok) {
                        throw new Error(`CSS response status: ${responseCss.status}`);
                    }
                    const blockJson = await responseJson.json();
                    const blockHtml = await responseHtml.text();
                    const blockCss  = await responseCss.text();
                    Sections.push({
                        data: blockJson,
                        css: blockCss,
                        html: blockHtml
                    });
                } catch (error) {
                    console.error(error.message);
                }
            }
        }

        async function loadSections() {
            await getCustomBlocks();
            Sections.forEach(item => {
                let SectionElement = document.createElement('div');
                SectionElement.classList.add('sectionPreview');
                SectionElement.setAttribute('data-content', item.html.trim());
                SectionElement.setAttribute('data-css', item.css.trim());
                
                // Créer la div pour l'image
                let imageDiv = document.createElement('div');
                let img = document.createElement('img');
                img.src = item.data.preview;
                imageDiv.appendChild(img);
                
                // Créer la div pour le nom de la section
                let sectionNameDiv = document.createElement('div');
                sectionNameDiv.classList.add('sectionName');
                sectionNameDiv.textContent = item.data.blockName;
                
                // Ajouter les deux divs au conteneur principal
                SectionElement.appendChild(imageDiv);
                SectionElement.appendChild(sectionNameDiv);
                
                // Ajouter le conteneur principal à assetsSections
                assetsSections.appendChild(SectionElement);
                
            });
        }
        loadSections();
    },

    /**
     * Reset the Frame
     */
    reset: function(){
        setTimeout(() => {
            sortableArea.destroy();
            SortableSections.destroy();
            SortableKits.destroy();
            Builder.Andromede.initComponents();
            Builder.Andromede.coreFrame();
            console.log('reset all sortables')
        }, 200);
    },

    /**
     * coreFrame
     */
    coreFrame: function(){

        let nestedSortables = [].slice.call(
            document.querySelectorAll('#andromede-editor *')
        );

        let sortableOptions = {
            group: {
                name: 'nested',
                put: ['nested', 'assetsKits']
            },
            animation: SpeedDrop,
            swapThreshold: 0.65,
            swap:false,
            revertOnSpill: true, 
            clone: true,
            ghostClass: 'grabbingElement',
            chosenClass: "sortableChosen", 
            dragClass: "sortableDrag",
            scroll: true,
            bubbleScroll: true,
            fallbackOnBody: true,
            emptyInsertThreshold: 5,
            bubble:true,
            forceFallback: true,
            onChoose: function (node) {
                Builder.Andromede.onSelected(node);
                StyleBuilder.nodeisSelected(node);
            },
            onStart: function (event) {
                Builder.Andromede.hideContextMenu(false, false)
                document.body.classList.add('currently_grap');
            },
            onEnd: function (event) {
                document.body.classList.remove('currently_grap');
                Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            }
        };

        let uniqueIdCounter = 1;
        for (let i = 0; i < nestedSortables.length; i++) {

            let element  = nestedSortables[i];
            let uniqueId = `${uniqueIdCounter}`;
            element.setAttribute('data-idblock', uniqueId);

            sortableArea = new Sortable(nestedSortables[i], sortableOptions);

            document.querySelectorAll('#andromede-editor *:not(.selectedBlock)').forEach(block => {
                block.addEventListener('mouseover', highlightParent);
                block.addEventListener('mouseout', removeHighlightParent);
            });

            function highlightParent(event) {
                event.currentTarget.classList.add('hoveredParent');
            }

            function removeHighlightParent(event) {
                event.currentTarget.classList.remove('hoveredParent');
            }

            uniqueIdCounter++;
        }
        Builder.Andromede.sections();
    },

    /**
     * Delete Component from Frame
     */
    deleteBlock(node) {
       Builder.Andromede.notificationSend('Block removed', false);
       Builder.Andromede.hideContextMenu(false, false);
       node.item.parentNode.removeChild(node.item)
       Builder.Andromede.reset();
    },

    /**
     * SelecteBlock on choose
     */
    onSelected: function(node){
        Builder.Andromede.removeSelected();
        Builder.Andromede.breadcrumb(node);
        const currentBlock = node.item;
        document.querySelectorAll('#andromede-editor *').forEach(block => {
            block.classList.remove('selectedBlock');
        });
        currentBlock.classList.add('selectedBlock');
        Builder.Andromede.showContextMenu(node, currentBlock);
        Builder.Andromede.selectedElement(node.item, node);
        Builder.Andromede.InsertBlock(node);
    },

    /**
     * Delete the currentBuilder
     */
    eraseBuilder: function(){
        BuilderArea.innerHTML = '';
        BuilderArea.innerHTML = `<andromede></andromede>`;
        Builder.Andromede.removeSelected();
        Builder.Andromede.reset();
        Builder.Andromede.notificationSend('The current document is now deleted', false);
    },

    /**
     * SetFrame HTML
     */
    setFrameContent: function(htmlContent){
        document.getElementById('andromede-editor').innerHTML = htmlContent;
        sortableArea.destroy();
        Builder.Andromede.coreFrame();
    },

    /**
     * Refresh the Frame
     */
    refreshFrame: function(){
        sortableArea.destroy();
        Builder.Andromede.modalClose();
        Builder.Andromede.coreFrame();
    },

    /**
     * Selected Element
     */
    selectedElement: function(block, node){
        selectedElement = node.item;
        selectedElementNode = node;
        selectedElementNodeID = parseInt(node.item.getAttribute('data-idblock'));
        return selectedElement, selectedElementNode, selectedElementNodeID;
    },

    /**
     * Remove Selected Block
     */
    removeSelected: function(){
        Builder.Andromede.hideContextMenu(false, false);
        document.querySelectorAll('#andromede-editor *').forEach(block => {
            block.classList.remove('selectedBlock');
            block.classList.remove('highlight');
        });
        Builder.Andromede.sections();
        isnoded.style.display = 'none';
    },

    /**
     * ShowContext Menu
     */
    showContextMenu: function(node, block){
        Builder.Andromede.hideContextMenu(node, block);
        const contextMenu = document.createElement('ul');
        contextMenu.classList.add('context-menu');

        let tagName    = block.tagName.toLowerCase();
        let firstClass = '';
        if (block.className) {
            const classes = block.className.split(' ');
            for (let i = 0; i < classes.length; i++) {
                if (classes[i] !== 'grab' 
                    && classes[i] !== 'hoveredParent' 
                    && classes[i] !== 'selectedBlock'
                    && !classes[i].startsWith('grab-')) {
                    firstClass = classes[i];
                    firstClass = '.'+firstClass;
                    break;
                }
            }
        }
    
        const options = [
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-copy"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M7 7m0 2.667a2.667 2.667 0 0 1 2.667 -2.667h8.666a2.667 2.667 0 0 1 2.667 2.667v8.666a2.667 2.667 0 0 1 -2.667 2.667h-8.666a2.667 2.667 0 0 1 -2.667 -2.667z" /><path d="M4.012 16.737a2.005 2.005 0 0 1 -1.012 -1.737v-10c0 -1.1 .9 -2 2 -2h10c.75 0 1.158 .385 1.5 1" /></svg>', 
                action: 'clone', 
                title: 'Cloner',
                target: block 
            },
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-circle-minus"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 12m-9 0a9 9 0 1 0 18 0a9 9 0 1 0 -18 0"/><path d="M9 12l6 0"/></svg>', 
                action: 'delete', 
                title: 'Supprimer',
                target: block 
            },
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-box-align-top"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 10.005h16v-5a1 1 0 0 0 -1 -1h-14a1 1 0 0 0 -1 1v5z" /><path d="M4 15.005v-.01" /><path d="M4 20.005v-.01" /><path d="M9 20.005v-.01" /><path d="M15 20.005v-.01" /><path d="M20 20.005v-.01" /><path d="M20 15.005v-.01" /></svg>', 
                action: 'upside', 
                title: 'Remonter',
                target: block 
            },
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-box-align-bottom"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 14h16v5a1 1 0 0 1 -1 1h-14a1 1 0 0 1 -1 -1v-5z" /><path d="M4 9v.01" /><path d="M4 4v.01" /><path d="M9 4v.01" /><path d="M15 4v.01" /><path d="M20 4v.01" /><path d="M20 9v.01" /></svg>', 
                action: 'bottom', 
                title: 'Descendre',
                target: block 
            },
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-schema"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M5 2h5v4h-5z" /><path d="M15 10h5v4h-5z" /><path d="M5 18h5v4h-5z" /><path d="M5 10h5v4h-5z" /><path d="M10 12h5" /><path d="M7.5 6v4" /><path d="M7.5 14v4" /></svg>', 
                action: 'parent', 
                title: 'Parent',
                target: block 
            },
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-html"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M13 16v-8l2 5l2 -5v8" /><path d="M1 16v-8" /><path d="M5 8v8" /><path d="M1 12h4" /><path d="M7 8h4" /><path d="M9 8v8" /><path d="M20 8v8h3" /></svg>', 
                action: 'html', 
                title: 'Edition HTML',
                target: block 
            },
            { 
                label: '<svg xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-article"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M3 4m0 2a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2z" /><path d="M7 8h10" /><path d="M7 12h10" /><path d="M7 16h10" /></svg><div style="text-align:center;font-size:10px;">Tiny</div>', 
                action: 'tinymode', 
                title: 'Edition Tiny',
                target: block 
            }
        ];
    
        options.forEach(optionData => {
            const li = document.createElement('li');
            li.innerHTML = optionData.label;
            li.title = optionData.title;
            li.dataset.action = optionData.action;
            li.addEventListener('click', function() {
                Builder.Andromede.handleOptionClick(optionData, block, node);
            });
            contextMenu.appendChild(li);
        });
    
        const rect = block.getBoundingClientRect();
        contextMenu.style.position = 'fixed';
        contextMenu.style.right = `0px`;
        document.body.appendChild(contextMenu);
        const blockCenterY = rect.top + (rect.height / 2);
        const contextMenuHeight = contextMenu.getBoundingClientRect().height;
        const topPosition = blockCenterY - (contextMenuHeight / 2);
        contextMenu.style.top = `${topPosition + 35}px`;

        // tagHover
        const tagNameDiv = document.createElement('div');
        tagNameDiv.textContent = block.tagName.toLowerCase() + ' ' + firstClass;
        tagNameDiv.classList.add('hoveredBlock');
        tagNameDiv.style.position = 'absolute';
        tagNameDiv.style.top = `${rect.top + window.scrollY}px`;
        tagNameDiv.style.right = `${window.innerWidth - (rect.left + window.scrollX + 12 + rect.width)}px`;
        document.body.appendChild(tagNameDiv);
    },
    
    /**
     * Hide Context menu
     */
    hideContextMenu(node, block) {
        const contextMenu = document.querySelector('.context-menu');
        if (contextMenu) {
            contextMenu.parentNode.removeChild(contextMenu);
        }
        const contextMenuhoveredBlock = document.querySelector('.hoveredBlock');
        if (contextMenuhoveredBlock) {
            contextMenuhoveredBlock.parentNode.removeChild(contextMenuhoveredBlock);
        }
        const contextMenuhoveredInsertBlock = document.querySelector('.addchildren');
        if (contextMenuhoveredInsertBlock) {
            contextMenuhoveredInsertBlock.parentNode.removeChild(contextMenuhoveredInsertBlock);
        }
    },

    /**
     * Execute action click
     */
    handleOptionClick: function(optionData, block, node){
        if (optionData.action === 'delete') {
            Builder.Andromede.deleteBlock(node);
            Builder.Andromede.hideContextMenu(node, block);
        }else if(optionData.action === 'clone'){
            Builder.Andromede.cloneBlock(node);
            Builder.Andromede.hideContextMenu(node, block);
        }else if(optionData.action === 'upside'){
            Builder.Andromede.moveBlockAbove(node);
        }else if(optionData.action === 'bottom'){
            Builder.Andromede.moveBlockBelow(node);
        }else if(optionData.action === 'parent'){
            Builder.Andromede.highlightParent(node);
        }else if(optionData.action === 'html'){
            Builder.Andromede.BlockopenEditorHTML(node);
        }else if(optionData.action === 'tinymode'){
            Builder.Andromede.BlockopenEditorTiny(node);
        }
        Builder.Andromede.breadcrumb(node);
    },

    /**
     * Hightlight Parent
     */
    highlightParent(node){
        const block = node.item;
        if (block instanceof HTMLElement) {
            const parent = block.parentNode;
            if (parent instanceof HTMLElement) {
                parent.classList.toggle('highlight');
            } else {
                console.error('Parent is not a valid HTML element:', parent);
            }
        } else {
            console.error('Invalid block:', block);
        }
    },

    /**
     * Move block to bottom
     */
    moveBlockBelow(node){
        const blockToMove = node.item;
        if (blockToMove instanceof HTMLElement) {
            const parent = blockToMove.parentNode;
            const nextSibling = blockToMove.nextElementSibling;
            if (nextSibling) {
                Builder.Andromede.notificationSend('Move block to below', false);
                parent.insertBefore(nextSibling, blockToMove);
                Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            } else {
                Builder.Andromede.notificationSend('The block is already the last child', false);
                console.warn('The block is already the last child.');
            }
        } else {
            console.error('Invalid block to move:', blockToMove);
        }
    },

    /**
     * Move block to upside
     */
    moveBlockAbove(node){
        const blockToMove = node.item;
        if (blockToMove instanceof HTMLElement) {
            const parent = blockToMove.parentNode;
            const previousSibling = blockToMove.previousElementSibling;
            if (previousSibling) {
                parent.insertBefore(blockToMove, previousSibling);
                Builder.Andromede.notificationSend('Move block to Above', false);
                Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            } else {
                Builder.Andromede.notificationSend('The block is already the first child', false);
                console.warn('The block is already the first child.');
            }
        } else {
            console.error('Invalid block to move:', blockToMove);
        }
    },

    /**
     * Insert a block beforeElement
     */
    InsertBlock: function(node){
        // let tagName    = node.item.tagName.toLowerCase();
        // if(tagName != 'img'){
        //     const rect = node.item.getBoundingClientRect();
        //     const insertBefore = document.createElement('div');
        //     insertBefore.innerHTML = `<div class="inserteur">
        //     <button title="Add a element before" data-action="click->buildermanager#addachildrenbefore">
        //         downtest
        //     </button></div>`;
        //     insertBefore.classList.add('addchildren');
        //     insertBefore.style.position = 'absolute';
        //     insertBefore.style.top = `${rect.bottom + window.scrollY + 3}px`;
        //     insertBefore.style.left = `${rect.left + window.scrollX + rect.width / 2}px`;
        //     insertBefore.style.transform = 'translateX(-50%)';
        //     insertBefore.style.transform = 'translateX(-50%)';
        //     document.body.appendChild(insertBefore);
        // }
    },

    /**
     * Breadcrumb
     */
    breadcrumb: function(node) {
        let breadcrumb = document.getElementById('breadcrumb');
        let path = [];
        let currentElement = node.item;
        path.push('<div class="breaditem" data-path="1">andromede</div>');
        let tempPath = [];
        let tempPathID = [];
        while (currentElement) {
            if (currentElement instanceof Element) {
                let tagName = currentElement.tagName.toLowerCase();
                let firstClass = '.'+currentElement.className.split(' ')[0];
                let nodeID = currentElement.getAttribute('data-idblock')
                if(firstClass == '.hoveredParent'){
                    firstClass = '';
                }
                tempPath.push(`${tagName}${firstClass}`);
                tempPathID.push(`${nodeID}`);
                if (tagName === 'div') {
                    break;
                }
                currentElement = currentElement.parentElement;
            } else {
                currentElement = null;
            }
        }
        tempPath.reverse();
        for (let i = 0; i < tempPath.length; i++) {
            let fullPath = tempPath.slice(0, i + 1).join(' > ');
            path.push(`<div class="breaditem" data-path="${tempPathID[i]}">${tempPath[i]}</div>`);
        }
        breadcrumb.innerHTML = path.join(' > ');
        let breaditems = breadcrumb.querySelectorAll('.breaditem');
        breaditems.forEach(item => {
            item.addEventListener('click', function() {
                let path = this.getAttribute('data-path');
                if (path) {
                    let selector = `[data-idblock="${path}"]`;
                    let targetElement = document.querySelector(selector);
                    if (targetElement) {
                        targetElement.scrollIntoView({
                            behavior: 'smooth',
                            block: 'start'
                        });
                    }
                }
            });
        });
    },
    
    /**
     * Clone Component
     */
    cloneBlock(node) {
        const blockToClone = node.item;
        if (blockToClone instanceof HTMLElement) {
            const clonedBlock = blockToClone.cloneNode(true);
            const parent = blockToClone.parentNode;
            if (parent) {
                parent.insertBefore(clonedBlock, blockToClone.nextSibling);
                Builder.Andromede.notificationSend('Block clone done', false);
                Builder.Andromede.sections();
                Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            }
        }
    },
    
    /**
     * Open CodeMirror
     */
    openEditorHTML(){
        let getCurrentContent =  document.getElementById('andromede-editor').innerHTML;
        editorBlock.classList.toggle('show_editor');
        let textarea = document.getElementById('code_editor');
        let removedClassInContent = [
            "hoveredParent",
            "selectedBlock"
        ];
        let regex = new RegExp(`\\b(${removedClassInContent.join('|')})\\b`, 'g');
        let cleanedContent = getCurrentContent.replace(regex, '');
        cleanedContent = cleanedContent.replace(/class="[^"]*"/g, function(match) {
            return match.replace(/\s{2,}/g, ' ').replace(/"\s+/, '"').replace(/\s+"/, '"');
        });
        cleanedContent = cleanedContent.replace(/\s*draggable="false"/g, '');
        cleanedContent = cleanedContent.replace(/\s*data-idblock="[^"]*"/g, '');
        cleanedContent = cleanedContent.replace(/<andromede.*?>/g, '');
        cleanedContent = cleanedContent.replace(/<\/andromede>/g, '');

        if (textarea) {
            editor.setSize("100%", '800');
            editor.setValue(html_beautify(cleanedContent,{
                "indent_inner_html": true,
                "indent_size": 1,
                "indent_char": " ",
                "brace_style": "collapse",
                "indent_scripts": "normal",
                "wrap_line_length": 0,
                "preserve_newlines": true,
                "max_preserve_newlines": 1
            }));
        }
    },

    openEditorCSS(){
        editorBlock.classList.toggle('show_editor');
        let textarea = document.getElementById('css_editor');
        let andromedeEditor = document.getElementById('andromede-editor');
        let styleTags = andromedeEditor.getElementsByTagName('style');
        let allCSS = '';
        for (let i = 0; i < styleTags.length; i++) {
            allCSS += styleTags[i].innerHTML;
        }
        if (textarea) {
            editor.setSize("100%", '800');
            editor.setOption("mode", 'css');
            editor.setOption("tabSize", '0');
            editor.setValue(allCSS);
        }
    },

    /**
     * Open TinyMCE 6
     */
    BlockopenEditorTiny(node) {
        Builder.Andromede.modalOpen(node);
        document.getElementById('modalcontent').innerHTML = `<textarea class="editorTiny" rows="10"></textarea>
        <div class="footermodal">
            <button class="saveBuilderBtn" id="saveHTMLfromblock">Appliquer</button>
        </div>`;
        tinymce.init({
            selector: '.editorTiny',
            language: 'fr_FR',
            deprecation_warnings: false,
            entity_encoding : "raw",
            a11y_advanced_options: true,
            force_br_newlines: false,
            force_p_newlines: false,
            forced_root_block : 'div',
            setup : function(ed) {
                ed.on('keyup', function (e) {    
                    const TinyMCEContent = tinymce.activeEditor.getContent();            
                });                    
            },
            branding: false,
            promotion: false,
            toolbar_sticky: true,
            editor_encoding: "raw",
            extended_valid_elements:"svg[*],defs[*],pattern[*],desc[*],metadata[*],g[*],mask[*],path[*],line[*],marker[*],rect[*],circle[*],ellipse[*],polygon[*],polyline[*],linearGradient[*],radialGradient[*],stop[*],image[*],view[*],text[*],textPath[*],title[*],tspan[*],glyph[*],symbol[*],switch[*],use[*]",
            height: 600,
            plugins: 'autosave preview template fullscreen code anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount searchreplace',
            toolbar: 'restoredraft undo redo | blocks fontsize  bold italic underline strikethrough forecolor backcolor code template link image media align | lineheight table  numlist bullist indent outdent emoticons charmap removeformat fullscreen searchreplace preview',
            image_title: true,
            automatic_uploads: true,
            autosave_interval: '10s',
            autosave_retention: '30m',
            content_style: '',
            image_caption: true,
            convert_urls: true,
            init_instance_callback: function (editor) {
                tinymce.activeEditor.setContent(node.item.outerHTML);
            }
        },
        document.addEventListener('focusin', (e) => {
            if (e.target.closest(".tox-tinymce-aux, .moxman-window, .tam-assetmanager-root") !== null) {
            e.stopImmediatePropagation();
        }}
        ));
        
        let saveHTMLfromblock = document.getElementById('saveHTMLfromblock');
        saveHTMLfromblock.addEventListener('click', function() {
            selectedElement.outerHTML = tinymce.activeEditor.getContent(".editorTiny");
            Builder.Andromede.removeSelected();
            Builder.Andromede.modalClose();
            Builder.Andromede.coreFrame();
            Builder.Andromede.notificationSend('Block change is done', false);
            Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            const tablist = document.getElementById('leftArea');
            if (tablist) {
                const buttons = tablist.querySelectorAll('.active');
                buttons.forEach(button => {
                    button.classList.remove('active');
                });
                tab1.classList.add('active');
                iscomponents.classList.add('active');
            }
        });
    },

    /**
     * Open CodeMirrorBlock
     */
    BlockopenEditorHTML(node) {
        Builder.Andromede.modalOpen(node);
        document.getElementById('modalcontent').innerHTML = `<textarea id="codeblock" rows="10"></textarea>
            <div class="footermodal">
                <button class="saveBuilderBtn" id="saveHTMLfromblock">Appliquer</button>
            </div>
        `;
        
        let codeBlock = document.getElementById('codeblock');
        if (node.item instanceof Element) {
            node.item = node.item.outerHTML;
        }
        codeBlock.value = node.item;

        /**
         * Clean & Regex
         */
        let getCurrentContent =  codeBlock.value;
        let removedClassInContent = [
            "hoveredParent",
            "selectedBlock"
        ];
        let regex = new RegExp(`\\b(${removedClassInContent.join('|')})\\b`, 'g');
        let cleanedContent = getCurrentContent.replace(regex, '');
        cleanedContent = cleanedContent.replace(/\s{2,}/g, ' ').trim();
        cleanedContent = cleanedContent.replace(/class="[^"]*"/g, function(match) {
            return match.replace(/\s{2,}/g, ' ').replace(/"\s+/, '"').replace(/\s+"/, '"');
        });
        cleanedContent = cleanedContent.replace(/\s*draggable="false"/g, '');
        cleanedContent = cleanedContent.replace(/\s*data-idblock="[^"]*"/g, '');

        let editBlock = CodeMirror.fromTextArea(document.getElementById('codeblock'), {
            mode: 'htmlmixed',
            autoIndent: true,
            indentUnit: 2,
            tabSize:2,
            enterMode: "keep",
            tabMode: "shift",
            lineNumbers:false,
            lineWrapping: true,
            theme:'material'
        });
        editBlock.setSize("100%", '600');

        setTimeout(() => {
            editBlock.setValue(html_beautify(cleanedContent,{
                "indent_inner_html": true,
                "indent_size": 1,
                "indent_char": " ",
                "brace_style": "collapse",
                "indent_scripts": "normal",
                "wrap_line_length": 0,
                "preserve_newlines": true,
                "max_preserve_newlines":1
            }));
        }, 10);

        let saveHTMLfromblock = document.getElementById('saveHTMLfromblock');
        saveHTMLfromblock.addEventListener('click', function() {
            selectedElement.outerHTML = editBlock.getValue();
            Builder.Andromede.removeSelected();
            Builder.Andromede.modalClose();
            sortableArea.destroy();
            Builder.Andromede.coreFrame();
            Builder.Andromede.notificationSend('HTML block is set', false);
            Backup.Backup.Andromede.init(BuilderArea.innerHTML);
            const tablist = document.getElementById('leftArea');
            if (tablist) {
                const buttons = tablist.querySelectorAll('.active');
                buttons.forEach(button => {
                    button.classList.remove('active');
                });
                tab1.classList.add('active');
                iscomponents.classList.add('active');
            }
        });
    },

    /**
     * Apply CodeMirror HTML to canva
     */
    saveHTML(){
        editorBlock.classList.toggle('show_editor');
        let newContentHtml = editor.getValue();
        newContentHtml = `<andromede>${newContentHtml}</andromede>`;
        Builder.Andromede.setFrameContent(newContentHtml);
        Builder.Andromede.sections();
    },

    getFrame: function(){
        let documentFrame = document.getElementById('andromede-editor').innerHTML;
        return documentFrame;
    },

    sections: function() {
        // Obtenir le HTML complet du document à partir de Builder.Andromede.getFrame()
        let html = Builder.Andromede.getFrame();
    
        // Créer un élément div temporaire pour analyser le HTML
        let tempDiv = document.createElement('div');
        tempDiv.innerHTML = html;
    
        // Sélectionner tous les éléments section et div.container-fluid dans votre document
        let elements = tempDiv.querySelectorAll('section');
    
        // Fonction récursive pour construire la structure de liste
        function buildListStructure(element) {
            let tagName = element.tagName.toLowerCase(); // Nom de la balise en minuscules
            let listItem = document.createElement('li');
            listItem.textContent = tagName;
    
            // Récupérer les enfants directs de l'élément
            let children = element.children;
            if (children.length > 0) {
                let ul = document.createElement('ul');
                ul.classList.add('collapse'); // Ajouter la classe collapse pour le collapsible
    
                // Parcourir chaque enfant direct de l'élément
                for (let i = 0; i < children.length; i++) {
                    let child = children[i];
                    let childListItem = buildListStructure(child);
                    ul.appendChild(childListItem);
                }
    
                listItem.appendChild(ul);

                listItem.addEventListener('click', function(e) {
                    e.stopPropagation(); // Empêcher la propagation de l'événement aux parents
                    ul.classList.toggle('show'); // Toggle pour afficher ou cacher la liste
                });
            }
    
            return listItem;
        }
    
        // Créer une liste <ul> principale avec la classe nodeElement
        let list = document.createElement('ul');
        list.classList.add('nodeElement');
    
        // Pour chaque élément section ou div.container-fluid trouvé, créer sa structure de liste
        elements.forEach(element => {
            let listItem = buildListStructure(element);
            list.appendChild(listItem);
        });
    
        // Ajouter la structure générée à l'élément avec l'ID sectionsDocument dans votre HTML
        let sectionsDocument = document.getElementById('sectionsDocument');
        sectionsDocument.innerHTML = ''; // Effacer le contenu existant
        sectionsDocument.appendChild(list);
    },

    /**
     * OpenModal
     */
    modalOpen: function(node){
        modalBuilder.style.display = "flex";
        let btnClose = document.getElementById("closeModal");
        btnClose.onclick = function() {
            modalBuilder.style.display = "none";
        }
    },

    /**
     * CloseModal
     */
    modalClose: function(){
        modalBuilder.style.display = "none";
    },

    /**
     * Shortcuts Actions
     */
    shortcuts: function(){
        document.addEventListener('keydown', function(event) {
            if (event.key === 'Delete') {
                Builder.Andromede.deleteBlock(selectedElementNode);
            }
        });
    },

    /**
     * Show a Notification
     */
    notificationSend: function(text, picture){
        Builder.Andromede.clearTimeouts();
        let notif = document.getElementById('notif');
        notif.style.display = 'block';
        let contentMsg = document.getElementById('content_msg');
        img_msg.src = "https://andromede-cms.com/medias/statics/large/2023-08/andromede2-2364da05-42.webp";
        contentMsg.innerHTML = text;
        timeoutIds.push(setTimeout(() => {
            notif.style.opacity = '1';
        }, 50));
        timeoutIds.push(setTimeout(() => {
            notif.style.opacity = '0';
        }, 2000));
        timeoutIds.push(setTimeout(() => {
            notif.style.display = 'none';
        }, 2500));
    },

    /**
     * Clean Timeout notification
     */
    clearTimeouts: function(){
        timeoutIds.forEach(timeoutId => clearTimeout(timeoutId));
        timeoutIds = [];
    },

    /**
     * Open Credits/Sources
     */
    Credits() {
        Builder.Andromede.modalOpen(false);
        document.getElementById('modalcontent').innerHTML = `<div class="credits">
            <div>Altitude Dev, <a href="https://altitude-dev.com/" target="_blank">Official Website</a></div>
            <div>Andromede CMS, <a href="https://andromede-cms.com/fr" target="_blank">Website</a></div>
            <div>----</div>
            <div>SortableJS, <a href="https://github.com/SortableJS" target="_blank">Github</a></div>
            <div>CodeMirror 5, <a href="https://codemirror.net/" target="_blank">Website</a></div>
            <div>Hotwired Stimulus, <a href="https://github.com/hotwired/stimulus" target="_blank">Github</a></div>
            <div>TinyMCE, <a href="https://github.com/tinymce/tinymce" target="_blank">Github</a></div>
            <div>JS Beautify, <a href="https://github.com/beautifier/js-beautify" target="_blank">Github</a></div>
            <div>----</div>
            <div>Florian Prache (FR), <a href="https://www.linkedin.com/in/altitude-dev/" target="_blank">Official Developper</a></div>
            <div>Gaetan Puelo (FR), <a href="https://www.linkedin.com/in/gaetanpuleo/" target="_blank">Contributor</a></div>
        </div>`;
    }
    
};

/**
 * Tabs
 */
document.addEventListener('DOMContentLoaded', function() {
    const tabs = document.querySelectorAll('.tab');
    const tabContents = document.querySelectorAll('.tab-content');
    tabs.forEach(tab => {
        tab.addEventListener('click', function() {
            const tabId = this.getAttribute('data-tab');
            tabs.forEach(tab => {
                tab.classList.remove('active');
            });
            tabContents.forEach(content => {
                content.classList.remove('active');
            });
            this.classList.add('active');
            const tabContent = document.getElementById(tabId);
            tabContent.classList.add('active');
        });
    });
});

/**
 * InitBuilder
 */
Builder.init();
export default {Builder}