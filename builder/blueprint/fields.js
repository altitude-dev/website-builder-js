/**
 * Andromede Builder
 * @copyright ALTITUDE DEV
 */

let Kits = {}

Kits.base = [
    {
        'name': 'text',
        'class': false,
        'category': 'base',
        'html': `
        <div><label>Text</label>
        <input type="text"></input></div>`
    }
];

export default {Kits}